import sys
sys.path.append('/Users/rosa/EFFORT/varpy_project')

from varpy.management import core , data_feed
from varpy.visualisation.histograms import rate_plots, mag_plots, model_plots
from varpy.data_preparation import window_preparation
from varpy.modelling.models import varpy_models

ID = 'DDS-7_v1'
#Attention: New!
d1=core.Laboratory(ID)

# The rest is the same until the last 3 lines ...
scld_data_file = 'Library/DDS-7_v1_scld_dataset.txt'
scld_metadata_file = 'Library/DDS-7_v1_scld_metadata.txt'
d1.add_attribute('scld',scld_data_file, scld_metadata_file)

print d1.scld.metadata
print d1.scld.dataset

rate_plots.scd_plot(d1, 'stress')
rate_plots.scd_plot(d1, 'axial_strain')
rate_plots.scd_plot(d1, 'porosity')


d2 = window_preparation.creep_phase(d1,'scld')
rate_plots.scd_plot(d2, 'stress')
rate_plots.scd_plot(d2, 'axial_strain')
rate_plots.scd_plot(d2, 'porosity')

#Attention: New
#d2.apply_model('scld','retro','axial_strain', d2.scld.metadata['t1'], 10000, d2.scld.metadata['tf'])
#d2.scld.display_models()
#model_plots.creep_scld_model_plot(d2, 'axial_strain')



