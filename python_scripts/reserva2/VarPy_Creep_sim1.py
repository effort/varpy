import sys
sys.path.append('/Users/rosa/EFFORT/varpy_project')

from varpy.management import core , data_feed
from varpy.visualisation.histograms import rate_plots, mag_plots, model_plots
from varpy.data_preparation import window_preparation
from varpy.modelling.models import varpy_models

ID = 'Sim1'

#Attention: New!
d1=core.Laboratory(ID)

t_start = 0.0
t_stop = 1099.9
t0 = 0.0
t1 = 100.
k1 = 50.0
c = 0.1
p1 = 0.9
k2 = 50.
t_finish = 1000.0
p2 = 0.9
mmin = 2.0
b = 1.0

d1.sim_attribute('ecld','creep_sim_lab',t_start, t_stop, t0, t1, k1, c, p1, k2, t_finish, p2, mmin, b)

rate_plots.rate_plot(d1)
mag_plots.mag_mc_plot(d1)

start = 100.0
finish = 1100.0

d2=window_preparation.datetime_window(d1, start, finish, 'ecld')

#Attention: New!
d2.apply_model('ecld', 'retro','exp_mle',500.0, 1000.0)
d2.ecld.display_models()

model_plots.exp_total_plot(d2,'ecld','exp_mle', 500.0, 1000.0)

rate_plots.rate_plot(d2)





